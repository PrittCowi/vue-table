Vue.component('maplistcomponent', {
    template: '#maplist-template',
    props: {
        data: Object,
        columns: Object,
        filterKey: String
    },
    data: function(){
        return{
            sortingKey: null,
            sortedData: null,
            sortKey: [],
            plsSort: null,
            order: -1,
            search: null,
            /* dropdown values*/
            visibleColumns: [],
            detailedView: null
        }
    },
    mounted: function(){
        var self = this;
        Object.keys(this.columns).forEach(function(heading){
            self.visibleColumns.push(heading);
        })
    },
    methods: {
        getTableData: function(){
            var test = Object.values(this.tableData);
            console.log(test);
        },
        removeHeading: function(value, key){
            console.log(value, key, this.tableData[key]);
            delete this.tableData[key]
            this.$forceUpdate()
        },
        sortBy: function (value, key) {
            console.log('hello world')
            if(this.order == -1){
                this.order = 1;
            }else if(this.order == 1){
                this.order = -1;
            }
            this.sortingKey = key;
        },
        findWithSearch: function(key){
            console.log(key)
            this.search = key;
        },
        clearSearch: function(){
            this.search = '';
        },
        updateColumns: function(){

        },
        updateDetailedView: function(geo){
            this.detailedView = geo.properties;
        }
    },
    computed:{
        filteredData: function () {
            let filterData = this.data.features;
            let order = this.order;
            let sortKey = this.sortingKey;

            let search = this.search && this.search.toLowerCase();
            if(search){
                /*filterData.features = filterData.features.filter(function(val){
                    return Object.keys(val.properties).some(function(propKey){
                        return String(val.properties[propKey]).toLowerCase().indexOf(search) > -1
                    });
                })*/
                filterData = filterData.filter(function(val){
                    return Object.keys(val.properties).some(function(propKey){
                        return String(val.properties[propKey]).toLowerCase().indexOf(search) > -1
                    });
                })
            }

            if(this.sortingKey != null){
                filterData.sort(function(val1, val2){
                    //console.log(val1.properties[sortKey]);
                    //console.log(val2.properties[sortKey]);
                    let a = val1.properties[sortKey].toUpperCase();
                    let b = val2.properties[sortKey].toUpperCase();
                    return  (a === b ? 0 : a > b ? 1 : -1) * order;
                })
            }
            
            if(search &&  filterData.length > 0){
                map.removeLayer(geom);
                geom = L.geoJson(filterData, {onEachFeature: handleFeatureClick}).addTo(map);
                map.fitBounds(geom.getBounds());
            }else if(typeof map != 'undefined' && filterData.length > 0){
                map.removeLayer(geom);
                geom = L.geoJson(filterData, {onEachFeature: handleFeatureClick}).addTo(map);
                map.fitBounds(geom.getBounds());
            }else if(typeof map != 'undefined'){
                map.removeLayer(geom);
            }
        return filterData;
    }}
});