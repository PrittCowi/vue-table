

var table = {
    "anv": "Anvendelse",
    "plannavn": "Plan navn"
}

var app = new Vue({
    el: '#maplist',
    data: function(){
        return {
            sortOrders : {},
            log: 'hello world',
            data: geoFeature,
            tableData: table,
            sortingKey: null,
            sortedData: null,
            sortKey: [],
            plsSort: null,
            order: -1,
            search: null
        }
    }
})

/*
var test = btoa('SELECT * FROM digitaleplaner.kursussted');
$.ajax({
    url: 'http://kort.digitaleplaner.dk/api/v1/sql/dk?q='+test,
    success: function(data){
        console.log(data);
    }
})
*/

function handleFeatureClick(feature, layer){
    
    layer.on('click', function(){
        app.$refs.maplist.search = feature.properties["plannavn"];
        console.log("hello world");
    })
}

var map = L.map('map').setView([51.505, -0.09], 13);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var geom = L.geoJson(geoFeature, {onEachFeature: handleFeatureClick}).addTo(map);
map.fitBounds(geom.getBounds());